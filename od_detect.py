# Two approaches to OD detection from
# doi:10.1109/tmi.2010.2053042

import cv2
import numpy as np
import matplotlib.pyplot as plt

### Low Pass Filter Method ###
# original image
f = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/Base34_edit/20051205_58010_0400_PP.tif', 0)
original = f

# image in frequency domain
F = np.fft.fft2(f)
Fshift = np.fft.fftshift(F)

# Filter: Low pass filter
M, N = f.shape
H = np.zeros((M, N), dtype=np.float32)
# cutoff frequency
D0 = 50
for u in range(M):
    for v in range(N):
        D = np.sqrt((u - M / 2) ** 2 + (v - N / 2) ** 2)
        if D <= D0:
            H[u, v] = 1
        else:
            H[u, v] = 0

# Ideal Low Pass Filtering
Gshift = Fshift * H

# Inverse Fourier Transform
G = np.fft.ifftshift(Gshift)

g = np.abs(np.fft.ifft2(G))

(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(g)
print(maxLoc)
marked = cv2.drawMarker(f, maxLoc, (0, 255, 255), markerType=cv2.MARKER_CROSS,
                        markerSize=40, thickness=2, line_type=cv2.LINE_AA)
cv2.circle(marked, maxLoc, 50, (255, 0, 0), 2)

plt.imshow(marked, cmap='gray')
plt.axis('off')
plt.show()

### Maximum Difference Method ###
height, width = f.shape

# replace black background with grey color (avoid large grayscale
# variance on the border of the retinal image)
for i in range(height):
    for j in range(width):
        if f[i, j] < 50:
            f[i, j] = 155

# Median Filtering
img_median = cv2.medianBlur(f, 21)

dif_max = 0
x_max_win = 0
y_max_win = 0
location = 0
windowSize = 150
stepSize = 150
image = original

# find a part of an image with maximum difference in pixel gray values
for y in range(0, image.shape[0], stepSize):
    for x in range(0, image.shape[1], stepSize):
        window = image[y:y + windowSize, x:x + windowSize]
        (minVal_w, maxVal_w, minLoc_w, maxLoc_w) = cv2.minMaxLoc(window)
        dif = maxVal_w - minVal_w
        if dif > dif_max:
            dif_max = dif
            location = window
            x_max_win = x
            y_max_win = y

cv2.rectangle(original, (x_max_win, y_max_win), (x_max_win + windowSize, y_max_win + windowSize),
              (255 / 3, 255 / 3, 255 / 3))

plt.imshow(f, cmap='gray')
plt.axis('off')
plt.show()




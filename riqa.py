import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
from PIL import Image
from scipy.signal import find_peaks

input_dir = "C:/Users/Aleksandra/PycharmProjects/image_quality/test_brightness"
output_dir_overexp = "C:/Users/Aleksandra/PycharmProjects/image_quality/overexp"
output_dir_norm = "C:/Users/Aleksandra/PycharmProjects/image_quality/norm"
output_dir_underexp = "C:/Users/Aleksandra/PycharmProjects/image_quality/underexp"

if not os.path.exists(output_dir_overexp):
    os.makedirs(output_dir_overexp)

if not os.path.exists(output_dir_underexp):
    os.makedirs(output_dir_underexp)

if not os.path.exists(output_dir_norm):
    os.makedirs(output_dir_norm)

for filename in os.listdir(input_dir):
    image = cv2.imread(os.path.join(input_dir, filename), 0)
    hist = cv2.calcHist([image], [0], None, [256], [0, 256])
    flattened_image = image.flatten()
    sorted_image = np.sort(flattened_image)
    flattened_image = flattened_image[flattened_image > 0]
    median = np.median(flattened_image)
    peaks, _ = find_peaks(np.squeeze(hist), height=200, distance=10)
    low_intens_limit = 10
    np.argmax(hist[low_intens_limit:])
    main_intensity = low_intens_limit + np.argmax(hist[low_intens_limit:])
    main_intensity
    image = Image.fromarray(image)
    if main_intensity < 70:
        image.save(os.path.join(output_dir_underexp, filename))
        print('Underexposed: ', filename)
    if 70 <= main_intensity < 130:
        image.save(os.path.join(output_dir_norm, filename))
    if main_intensity >= 130:
        image.save(os.path.join(output_dir_overexp, filename))
        print('Overexposed: ', filename)


import cv2
import numpy as np

# Load the image in greyscale
img = cv2.imread('D:\DRIMDB\DRIMDB\Good\drimdb_good (32).jpg', 0)

# Apply Gaussian Blur
blur = cv2.GaussianBlur(img, (9, 9), 0)

# Apply Laplacian operator in some higher datatype
laplacian = cv2.Laplacian(blur, cv2.CV_64F, ksize=5)
print(laplacian.max())
print(laplacian.min())
lap_var = laplacian.var()
print(lap_var)

cv2.imshow('a7', laplacian)
cv2.waitKey(0)

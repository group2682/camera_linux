import cv2
import numpy
import numpy as np
import imutils
from PIL.ImageQt import ImageQt
from PIL import Image
import ximea
from PyQt5.QtGui import QImage
from matplotlib import cm
from ximea import xiapi
from ximea.xiapi import Camera

import sys
from PyQt5 import QtWidgets, QtCore, QtGui
import interface
import os
import PIL.Image
import tkinter as tk
import os

gain_v = 0
exp_v = 0
global save_path


class MyThread(QtCore.QThread):
    mysignal = QtCore.pyqtSignal(str)
    global gain_v
    global exp_v
    global save_path
    cam: Camera

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

    def run(self) -> None:
        print('XIMEA version: ', ximea.__version__)
        self.mysignal.emit(f'XIMEA version: {ximea.__version__}')
        # increment to number taken pictures
        increment_val = 0
        i = 0
        photos = []
        numbers = []

        # settings
        # input is used as trigger
        self.cam.set_gpi_mode('XI_GPI_TRIGGER')
        # set trigger - rising/falling edge
        self.cam.set_trigger_source('XI_TRG_EDGE_RISING')
        self.cam.set_trigger_selector('XI_TRG_SEL_FRAME_START')
        # set data format
        self.cam.set_imgdataformat('XI_MONO8')
        # set gain and exposure
        self.cam.set_gain(gain_v)
        self.cam.set_exposure(exp_v)

        print('Exposure was set to %i us' % self.cam.get_exposure())
        print('Gain was set to %i us' % self.cam.get_gain())
        self.mysignal.emit('Exposure was set to %i us' % self.cam.get_exposure())
        self.mysignal.emit('Gain was set to %i dB' % self.cam.get_gain())

        # create instance of Image to store image data and metadata
        img = xiapi.Image()
        # start data acquisition
        print('Starting data acquisition...')
        self.mysignal.emit('Starting data acquisition...')
        self.cam.start_acquisition()

        try:
            while True:
                # get data and pass them from camera to img
                # just ignore the #0 picture
                self.cam.get_image(img)
                # get raw data from camera
                img_data = img.get_image_data_numpy()

                # save acquired image
                print('Taking image...')
                self.mysignal.emit('Taking image...')
                numbers.append(increment_val)
                photos.append(img_data)
                # print and increase increment (picture No)
                print(str(increment_val))
                self.mysignal.emit(str(increment_val))
                increment_val += 1
                # Framerate
                framerate = self.cam.get_framerate()
                fr_max = self.cam.get_framerate_maximum()
                fr_min = self.cam.get_framerate_minimum()
                print('Framerate: ' + str(framerate) + '\n Fr max: ' + str(fr_max) + '\n Fr min: ' + str(fr_min) + '\n')

        except Exception as inst:
            print(type(inst))
            print(inst.args)
            print(inst)
            self.mysignal.emit('Process finished')
        finally:
            print('Stopping acquisition...')
            self.mysignal.emit('Stopping acquisition...')
            self.cam.stop_acquisition()
            # stop communication
            self.cam.close_device()
            print('Saving photos:')
            self.mysignal.emit('Saving photos:')

            while i < len(photos):
                # os path join - latest created folder
                cv2.imwrite(os.path.join(save_path, 'picture' + str(numbers[i]) + '.png'), photos[i])
                self.mysignal.emit('Photo %i ' % numbers[i])
                i += 1

            print('Done!')
            self.mysignal.emit('Done!')


# class to use functions of user interface file
class ExampleApp(QtWidgets.QMainWindow, interface.Ui_MainWindow):
    def __init__(self):
        global gain_v
        global exp_v
        global save_path

        # to access variables, methods, etc in file interface.py
        super().__init__()
        self.setupUi(self)  # initialization of UI
        self.resultsButton.clicked.connect(self.open_folder)
        self.startButton.clicked.connect(self.init_start)
        self.create_folder_button.clicked.connect(self.create_folder())

        self.cam: Camera = xiapi.Camera()
        print('Opening camera...')
        self.cam.open_device()
        self.cam.set_gpi_mode('XI_GPI_OFF')
        self.cam.set_acq_timing_mode('XI_ACQ_TIMING_MODE_FREE_RUN')
        self.cam.disable_aeag()
        self.cam.set_gain(1)
        self.cam.set_exposure(25000)
        self.cam.start_acquisition()

        self.preview_timer = QtCore.QTimer()
        self.preview_timer.timeout.connect(self.render_preview)
        self.preview_timer.setInterval(30)
        self.preview_timer.setSingleShot(False)
        self.preview_timer.start()

        # create our thread
        self.my_thread = MyThread(self)
        self.my_thread.cam = self.cam
        self.my_thread.mysignal.connect(self.update_list)
        self.my_thread.started.connect(self.on_start)
        self.my_thread.finished.connect(self.on_finish)

    # function open folder
    def open_folder(self):
        directory = 'C:\\Users\\Aleksandra\\PycharmProjects\\camera'
        path = os.path.realpath(directory)
        os.startfile(path)

    def create_folder(self):
        window = tk.Tk()
        window.title("Create folder")
        frame = tk.Frame(window)
        frame.pack()
        entry = tk.Entry(frame)
        entry.pack()

        def get_input():
            global save_path
            folder_name = entry.get()
            current_directory = os.getcwd()
            try:
                print(folder_name)
                save_path = os.path.join(current_directory, folder_name)
                os.makedirs(os.path.join(current_directory, folder_name))
            except:
                print('Folder already exists')
            window.destroy()

        button = tk.Button(frame, text="OK", command=get_input)
        button.pack()
        window.mainloop()

    # when start button is pressed - start our thread
    def init_start(self):
        global gain_v, exp_v
        self.preview_timer.stop()
        gain_v = self.user_gain.value()
        exp_v = self.user_exposure.value()
        self.listWidget.clear()  # clear the list
        self.cam.stop_acquisition()
        if self.cam.get_acquisition_status() == 'XI_OFF':
            self.my_thread.start()

    # if signal comes, add the string to the list
    def update_list(self, msg: str):
        self.listWidget.addItem(msg)
        self.listWidget.scrollToBottom()

    # button, checkbox are disabled while camera is running
    def on_start(self):
        self.startButton.setEnabled(False)
        self.user_gain.setEnabled(False)
        self.user_exposure.setEnabled(False)
        self.preview_timer.stop()

    # button, checkbox are enabled back when the process finished
    def on_finish(self):
        self.startButton.setEnabled(True)
        self.user_gain.setEnabled(True)
        self.user_exposure.setEnabled(True)
        self.cam.open_device()
        if self.cam.get_acquisition_status() == 'XI_OFF':
            self.cam.set_gpi_mode('XI_GPI_OFF')
            self.cam.set_acq_timing_mode('XI_ACQ_TIMING_MODE_FREE_RUN')
            self.cam.disable_aeag()
            self.cam.set_exposure(9741)
            self.cam.set_gain(5)  # dB
            self.cam.start_acquisition()
        self.preview_timer.start()

    def render_preview(self):
        img = xiapi.Image()
        self.cam.get_image(img)
        data = img.get_image_data_numpy()
        # cv2.imwrite('test_picture.png', data)
        # clahe = cv2.createCLAHE()
        # cl1 = clahe.apply(data)
        enhanced = cv2.addWeighted(data, 1, data, 0.5, 0.0)
        # create a circular mask
        circle = np.zeros((1024, 1280), dtype="uint8")
        cv2.circle(circle, (630, 520), 580, 255, -1)
        masked = cv2.bitwise_and(enhanced, circle)
        raw = PIL.Image.fromarray(masked, 'L')
        #
        blurr = cv2.GaussianBlur(masked, (9, 9), 0)
        # avg = np.mean(data, axis=(0, 1))
        # print(avg)
        thr, thr_img = cv2.threshold(blurr, 140, 255, cv2.THRESH_BINARY)
        er_it = self.erosion_slider.value()
        dl_it = self.dilation_slider.value()
        erosion = cv2.morphologyEx(thr_img, cv2.MORPH_ERODE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
                                   iterations=er_it)
        dilation = cv2.morphologyEx(erosion, cv2.MORPH_DILATE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
                                    iterations=dl_it)
        self.itr_lbl.setText('E %d D %d' % (er_it, dl_it))
        # setup parameters for blob detection
        params = cv2.SimpleBlobDetector_Params()
        params.minThreshold = 10
        params.maxThreshold = 255
        params.filterByArea = True
        params.minArea = 5000
        params.maxArea = 100000
        params.filterByCircularity = True
        params.maxCircularity = 1
        params.minCircularity = 0.3
        params.filterByConvexity = False
        params.filterByInertia = True
        params.maxInertiaRatio = 0.9
        params.minInertiaRatio = 0.4
        # create the detector with parameters
        detector = cv2.SimpleBlobDetector_create(params)
        thresh_array = np.invert(np.asarray(dilation))  # INVERT!
        raw_array = np.asarray(raw)  # masked original image
        # detect blob on the thresholded picture
        keypoint = detector.detect(thresh_array)
        x_marker = self.horizontalMarker.value()
        y_marker = self.verticalMarker.value()
        # text labels with marker coordinates
        self.mark_pos_h.setText(str(x_marker))
        self.mark_pos_v.setText(str(y_marker))
        # take max keypoint and check if its position fits with the marker
        if keypoint:
            for keyp in keypoint:
                x = keyp.pt[0]
                y = keyp.pt[1]
                if (x < x_marker - 5 and y > y_marker + 5) or (x < x_marker - 5 and y < y_marker - 5) \
                        or (x > x_marker + 5 and y > y_marker + 5) or (x > x_marker + 5 and y < y_marker - 5):
                    self.od_pos_actual.setText('OD x: %d y: %d' % (x, y))
        else:
            self.od_pos_actual.setText('OD not found')

        img_with_keypoint = cv2.drawKeypoints(raw_array, keypoint, np.array([]), (0, 0, 255),
                                              cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        marked = cv2.drawMarker(img_with_keypoint, [x_marker, y_marker], (0, 255, 255), markerType=cv2.MARKER_CROSS,
                                markerSize=40, thickness=2, line_type=cv2.LINE_AA)
        thr_data = QtGui.QImage(marked.data, img_with_keypoint.shape[1],
                                img_with_keypoint.shape[0], QtGui.QImage.Format_RGB888).rgbSwapped()
        self.preview_label.setPixmap(QtGui.QPixmap.fromImage(thr_data))

        ### DEBUG - show thresholded image
        im = Image.fromarray(np.uint8(cm.gist_earth(thresh_array) * 255))
        pixmap = self._convert_to_pixmap(im)
        self.label_3.setPixmap(pixmap)


    @staticmethod
    def _convert_to_pixmap(pillow_image):
        qim = ImageQt(pillow_image)
        return QtGui.QPixmap.fromImage(qim)


# new QApplication
app = QtWidgets.QApplication(sys.argv)
# create object of class ExampleApp
window = ExampleApp()
# show the window
window.show()
# and start the application
app.exec_()

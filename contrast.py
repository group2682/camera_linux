from PIL import Image
import cv2
from PIL import ImageEnhance, ImageFilter
import os
import random

input_dir = "C:/Users/Aleksandra/PycharmProjects/image_quality/Base34"
output_dir = "C:/Users/Aleksandra/PycharmProjects/image_quality/test_brightness"

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

mask = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/mask.png', 0)
width, height = mask.shape

# Iterate through all files in the input directory
for filename in os.listdir(input_dir):
    # Open the image
    image = Image.open(os.path.join(input_dir, filename))
    # image = cv2.resize(image, (height, width), interpolation=cv2.INTER_AREA)
    # image = Image.fromarray(image)
    image = image.convert("L")
    # blur = random.randint(0, 10)
    blur = random.randint(0, 10 )
    image = image.filter(ImageFilter.GaussianBlur(blur))
    mask = image.point(lambda x: 0 if x < 10 else 255, "1")
    k = random.randint(0, 1)

    if k:
        # Generate a random brightness value between 2 and 2.5
        # brightness = random.uniform(2, 2.5)
        brightness = random.uniform(1, 1.5)
        enhancer = ImageEnhance.Brightness(image)
        image = enhancer.enhance(brightness)  # increase brightness
        # Create an output image with a white background
        output = Image.new("RGB", image.size, (0, 0, 0))
        output.paste(image, mask=mask)
        # Save the output image
        output.save(os.path.join(output_dir, filename))
    else:
        # Generate a random brightness value between 1.5 and 2
        # brightness = random.uniform(0.8, 0.3)
        brightness = random.uniform(1.5, 2)
        enhancer = ImageEnhance.Brightness(image)
        image = enhancer.enhance(brightness)  # increase brightness
        # Create an output image with a white background
        output = Image.new("RGB", image.size, (0, 0, 0))
        output.paste(image, mask=mask)
        # Save the output image
        output.save(os.path.join(output_dir, filename))


#
# # BRIGHTENING
# image = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/Base34/20051208_41570_0400_PP.tif', 0)
# mask = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/mask.png', 0)
# width, height = mask.shape
# image = cv2.resize(image, (height, width), interpolation=cv2.INTER_AREA)
#
# # convert array to image object
# image = Image.fromarray(image)
# mask = Image.fromarray(mask)
# # Create a binary mask where pixel intensity is less than 10
# mask = image.point(lambda x: 0 if x < 10 else 255, "1")
#
# enhancer = ImageEnhance.Brightness(image)
# image = enhancer.enhance(2)  # increase brightness
#
# # Create an output image with a white background
# output = Image.new("RGB", image.size, (0, 0, 0))
#
# # Paste the input image onto the output image using the mask
# output.paste(image, mask=mask)
#
# # Save the output image
# output.save("brightened_image.jpg")
#
# # DARKENING
# image_2 = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/Base34/20051205_58458_0400_PP.tif', 0)
# image_2 = cv2.resize(image_2, (height, width), interpolation=cv2.INTER_AREA)
# image_2 = Image.fromarray(image_2)
# mask = image_2.point(lambda x: 0 if x < 10 else 255, "1")
#
# # Decrease the brightness of the image using the ImageEnhance module
# enhancer = ImageEnhance.Brightness(image_2)
# image_2 = enhancer.enhance(0.3)  # decrease brightness by 50%
# output = Image.new("RGB", image_2.size, (0, 0, 0))
#
# # Save the output image
# image_2.save("darkened_image.jpg")


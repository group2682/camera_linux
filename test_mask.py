# test binary mask from retinal picture from https://doi.org/10.1016/j.inffus.2012.08.001

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
from PIL import Image

input_dir = "C:/Users/Aleksandra/Desktop/7_02_new_eye/2"
output_dir = "C:/Users/Aleksandra/Desktop/7_02_new_eye/2/masked"
output_dir_mask = "C:/Users/Aleksandra/Desktop/7_02_new_eye/2/mask"

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if not os.path.exists(output_dir_mask):
    os.makedirs(output_dir_mask)

import glob
path = input_dir

files = glob.glob(os.path.join(path, '*.tif'))

for f in files:
    gray_image = cv2.imread(f, 0)
    mask = np.where(gray_image >= 10, 255, 0)
    kernel = np.ones((6, 6), np.uint8)
    mask = mask.astype(np.uint8)
    opening_mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    masked_img = gray_image * opening_mask
    image = Image.fromarray(masked_img)
    mask = Image.fromarray(mask)
    filename = os.path.basename(f).split('/')[-1]
    print(filename)
    mask.save(os.path.join(output_dir_mask, filename))
    image.save(os.path.join(output_dir, filename))
    # mask.save(os.path.join(output_dir_mask), os.path.basename(path).split('/')[-1])


import subprocess
import os
import cv2
from skimage.exposure import is_low_contrast
from PIL import Image
import numpy as np
from scipy.fftpack import fft2, fftshift
import pywt
import matplotlib.pyplot as plt

import riqa

dir_norm = "C:/Users/Aleksandra/PycharmProjects/image_quality/norm"
out_dir_blurry = "C:/Users/Aleksandra/PycharmProjects/image_quality/blurry"

if not os.path.exists(out_dir_blurry):
    os.makedirs(out_dir_blurry)

for filename in os.listdir(dir_norm):
    image = cv2.imread(os.path.join(dir_norm, filename), 0)
    contrast = is_low_contrast(image, fraction_threshold=0.35)
    ###
    # dim = (640, 480)
    # image = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    ###
    var = cv2.Laplacian(image, cv2.CV_64F, ksize=5).var()
    ###
    # ft = fft2(image)
    # ft_shifted = fftshift(ft)
    # magnitude = np.abs(ft_shifted)
    # mean_magnitude = np.mean(magnitude)
    ###
    # coeffs = pywt.wavedec2(image, 'db1', level=3)
    # magnitude = [np.abs(c).mean() for c in coeffs]
    # mean_magnitude = np.mean(magnitude)
    # cv2.imshow('image', cv2.resize(image, (680, 500)))
    # cv2.waitKey(0)
    # print('mean magnitude: ', mean_magnitude)
    # print('laplace', var)
    ###
    if contrast or var < 700:
    # if contrast or mean_magnitude < 4000:
        # cv2.imshow('low contrast or blurry', cv2.resize(image, (680, 500)))
        # cv2.waitKey(0)
        image = Image.fromarray(image)
        image.save(os.path.join(out_dir_blurry, filename))
        os.remove(os.path.join(dir_norm, filename))
        # print('contrast ', contrast)
        # print('laplace ', var)
        print('low contrast or blurry - removed')




import cv2
import numpy as np
from PIL import Image
import os
import matplotlib.pyplot as plt

# Masking from paper:
# Retinal image quality assessment using generic image quality indicators.
# https://doi.org/10.1016/j.inffus.2012.08.001

img = cv2.imread('D:/STARE_tif/im0009.tiff',
                 cv2.IMREAD_GRAYSCALE)

mask = np.where(img >= 10, 1, 0)
kernel = np.ones((6, 6), np.uint8)
opening_mask = cv2.morphologyEx(np.uint8(mask), cv2.MORPH_OPEN, kernel)
masked_img = img * opening_mask
image = Image.fromarray(masked_img)

plt.imshow(masked_img, cmap='gray')
plt.imshow(opening_mask, cmap='gray')


# thr, thr_img = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY)
# opening = cv2.morphologyEx(thr_img, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
#                            iterations=2)
# cv2.imshow('Opening', opening)
# cv2.waitKey(0)

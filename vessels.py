import cv2
import numpy as np
import sys
from PIL import Image
from matplotlib import cm
from PIL.ImageQt import ImageQt
from PyQt5 import QtWidgets, QtGui
import controls

global resized
global skeleton

img = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/Base34_edit/20051208_39243_0400_PP.tif')
green_channel = img[:, :, 1]
dim = (2304, 1576)
# resize image
resized = cv2.resize(green_channel, dim, interpolation=cv2.INTER_AREA)

# remove the noise on the black background
for i in range(resized.shape[0]):
    for j in range(resized.shape[1]):
        if resized[i, j] < 11:
            resized[i, j] = 0

cv2.imshow('Input', resized)
cv2.waitKey(0)


def save_img():
    cv2.imwrite('vessels_skeleton.png', skeleton)


class Vessels(QtWidgets.QMainWindow, controls.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  # initialization of UI

        self.kernel_1.valueChanged.connect(self.render_pic)
        self.kernel_2.valueChanged.connect(self.render_pic)
        self.dil_1_iter.valueChanged.connect(self.render_pic)
        self.ern_1_iter.valueChanged.connect(self.render_pic)
        self.dil_2_iter.valueChanged.connect(self.render_pic)
        self.ern_2_iter.valueChanged.connect(self.render_pic)
        self.median_k.valueChanged.connect(self.render_pic)
        self.kernel_3.valueChanged.connect(self.render_pic)
        self.kernel_4.valueChanged.connect(self.render_pic)
        self.ern_3_iter.valueChanged.connect(self.render_pic)
        self.ern_4_iter.valueChanged.connect(self.render_pic)
        self.thresh.valueChanged.connect(self.render_pic)
        self.saveButton.clicked.connect(save_img)

    def render_pic(self):
        global skeleton

        kernel1 = self.kernel_1.value()
        dil_1_itr = self.dil_1_iter.value()
        ern_1_itr = self.ern_1_iter.value()
        kernel2 = self.kernel_2.value()
        dil_2_itr = self.dil_2_iter.value()
        ern_2_itr = self.ern_2_iter.value()
        median_kern = self.median_k.value()
        kernel3 = self.kernel_3.value()
        kernel4 = self.kernel_4.value()
        ern_3_itr = self.ern_3_iter.value()
        ern_4_itr = self.ern_4_iter.value()
        threshold = self.thresh.value()

        self.kern_1_lbl.setText(str(kernel1))
        self.dil_1_lbl.setText(str(dil_1_itr))
        self.ern_1_lbl.setText(str(ern_1_itr))
        self.kern_2_lbl.setText(str(kernel2))
        self.dil_2_lbl.setText(str(dil_2_itr))
        self.ern_2_lbl.setText(str(ern_2_itr))
        self.kern_3_lbl.setText(str(kernel3))
        self.kern_4_lbl.setText(str(kernel4))
        self.ern_3_lbl.setText(str(ern_3_itr))
        self.ern_4_lbl.setText(str(ern_4_itr))
        self.thresh_lbl.setText(str(threshold))

        kernel_1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel1, kernel1))
        dilation_1 = cv2.dilate(resized, kernel_1, iterations=dil_1_itr)
        erosion_1 = cv2.erode(dilation_1, kernel_1, iterations=ern_1_itr)

        kernel_2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel2, kernel2))
        dilation_2 = cv2.dilate(resized, kernel_2, iterations=dil_2_itr)
        erosion_2 = cv2.erode(dilation_2, kernel_2, iterations=ern_2_itr)

        c = erosion_2 - erosion_1
        ret, thresh1 = cv2.threshold(c, threshold, 255, cv2.THRESH_BINARY)
        u = cv2.medianBlur(thresh1, median_kern)
        u_2 = np.invert(u)

        kernel_3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel3, kernel3))
        kernel_4 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel4, kernel4))
        erosion_3 = cv2.erode(u, kernel_3, iterations=ern_3_itr)
        erosion_4 = cv2.erode(u_2, kernel_4, iterations=ern_4_itr)

        skeleton = u - (erosion_3 - erosion_4)
        skeleton = cv2.medianBlur(skeleton, 1)

        im = Image.fromarray(np.uint8(cm.gist_earth(skeleton) * 255))
        pixmap = self._convert_to_pixmap(im)
        self.result.setPixmap(pixmap)

    @staticmethod
    def _convert_to_pixmap(pillow_image):
        qim = ImageQt(pillow_image)
        return QtGui.QPixmap.fromImage(qim)


# new QApplication
app = QtWidgets.QApplication(sys.argv)
# create object of class ExampleApp
window = Vessels()
# show the window
window.show()
# and start the application
app.exec_()

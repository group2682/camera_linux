import scipy.optimize
import math
import numpy as np
import matplotlib.pyplot as plt


def parabola(X, theta, p, xc, yc):
    x, y = X
    res = ((x - xc) * math.sin(theta) + (y - yc) * math.cos(theta)) ** 2 - 2 * p * (
                (x - xc) * math.cos(theta) - (y - yc) * math.sin(theta))
    return res


def rotated_parabola(X, a, alpha):
    x, y = X
    dx = x-xc
    dy = y-yc

    x_l = math.cos(alpha)*dx - math.sin(alpha)*dy
    y_l = math.sin(alpha)*dx + math.cos(alpha)*dy

    res = y_l - (a*(x_l)**2)

    return res

def sample_parabola(x_l, a, alpha, xc, yc):
    y_l = (a * (x_l) ** 2)

    x = math.cos(alpha) * x_l + math.sin(alpha) * y_l
    y = -math.sin(alpha) * x_l + math.cos(alpha) * y_l

    x += xc
    y += yc

    return x, y

# vertex
# if known, set as global, do not input to "parabola" func
xc = 100
yc = 100

val = np.asarray([0, ])
# points from Hough lines
x = np.asarray([xc + 10, xc-10])
y = np.asarray([yc + 100, yc+100])

bounds = ([0.3, -np.inf], [np.inf, np.inf])

param, param_cov = scipy.optimize.curve_fit(rotated_parabola, (x, y), val, method='dogbox', bounds=bounds)
# angle, focal, vertex x,y
print(param[0], math.degrees(param[-1]))







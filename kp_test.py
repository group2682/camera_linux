import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread('D:\DRIMDB\DRIMDB\Good\drimdb_good (32).jpg', 0)
mask = np.where(img >= 20, 1, 0)
kernel = np.ones((6,6),np.uint8)
opening_mask = cv2.morphologyEx(np.uint8(mask), cv2.MORPH_OPEN, kernel)

sift = cv2.xfeatures2d.SIFT_create()
keypoints, descriptors = sift.detectAndCompute(img, opening_mask)
print("Number of keypoints: ", len(keypoints))
print("Descriptor dimensions: ", descriptors.shape)

img_kp = img.copy()
resultimage = cv2.drawKeypoints(img, keypoints, 0, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# cv2.drawKeypoints(img, keypoints, img_kp, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
cv2.imshow('Keypoints', resultimage)
cv2.waitKey(0)

cv2.imwrite('im_kp_4.png', resultimage)

import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
import math

global avg_x
global avg_y

# Hough lines algorithm - vessels detection
skeleton = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/vessels_skeleton.png', 0)
height, width = skeleton.shape

mask = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/mask.png', 0)

thr, thr_img = cv2.threshold(mask, 11, 255, cv2.THRESH_BINARY)
opening = cv2.morphologyEx(thr_img, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
                           iterations=2)
# threshold
for i in range(height):
    for j in range(width):
        if skeleton[i, j] < 250:
            skeleton[i, j] = 0
        else:
            skeleton[i, j] = 255


threshold = 10
theta = 5

# detect lines with Hough algorithm
lines = cv2.HoughLinesP(skeleton, rho=1, theta=np.pi / 180 * int(theta), threshold=int(threshold),
                        minLineLength=30, maxLineGap=2)

final_lines = []
intersection_points = []
final = skeleton

max_length_1 = 0
max_length_2 = 0
max_line_1 = 0
max_line_2 = 0
# store the points of the lines
mid_lines_x = []
mid_lines_y = []

# Draw the lines
if lines is not None:
    for i in range(0, len(lines)):
        l = lines[i][0]
        cv2.line(skeleton, (l[0], l[1]), (l[2], l[3]), (255 / 2, 255 / 2, 255 / 2), 1, cv2.LINE_AA)
        tg = (l[3] - l[1]) / (l[2] - l[0])
        angle = math.atan(tg) * 57.3
        if angle < 0:
            angle = 180 - abs(angle)
        # leave only lines with angles between 45 and 135 degrees
        if 45 < angle < 135:
            final_lines.append(l)
            # middle of the line: ((l[0] + l[2]) / 2, (l[1] + l[3]) / 2)
            mid_lines_x.append((l[0] + l[2]) / 2)
            mid_lines_y.append((l[1] + l[3]) / 2)
            final = cv2.drawMarker(final, (int((l[0] + l[2]) / 2), int((l[1] + l[3]) / 2)), (255, 255, 255),
                                   markerType=cv2.MARKER_STAR, markerSize=10, thickness=1, line_type=cv2.LINE_AA)
            if 45 < angle < 85:
                length_1 = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
                if length_1 > max_length_1:
                    max_length_1 = length_1
                    max_line_1 = l
            if 95 < angle < 135:
                length_2 = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
                if length_2 > max_length_2:
                    max_length_2 = length_2
                    max_line_2 = l

plt.imshow(final)
plt.axis('off')
plt.show()

def intersection(L1, L2):
    D = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    else:
        return False


all_x = []
all_y = []

# Calculate the intersection points
for line1 in final_lines:
    A1 = (line1[1] - line1[3])
    B1 = (line1[2] - line1[0])
    C1 = (line1[0] * line1[3] - line1[2] * line1[1])
    L1 = (A1, B1, -C1)
    for line2 in final_lines:
        if True:
            A2 = (line2[1] - line2[3])
            B2 = (line2[2] - line2[0])
            C2 = (line2[0] * line2[3] - line2[2] * line2[1])
            L2 = (A2, B2, -C2)
            R = intersection(L1, L2)
            if R:
                if width > R[0] > 0 and height > R[1] > 0:
                    all_x.append(R[0])
                    all_y.append(R[1])
            else:
                continue

avg_x = np.sum(all_x) / len(all_x)
avg_y = np.sum(all_y) / len(all_y)


def parabola(X, theta_p, p):
    x, y = X
    res = ((x - avg_x) * math.sin(theta_p) + (y - avg_y) * math.cos(theta_p)) ** 2 - 2 * p * (
            (x - avg_x) * math.cos(theta_p) - (y - avg_y) * math.sin(theta_p))
    return res


# this function was used for parabola fitting
def rotated_parabola(X, a, alpha):
    x, y = X
    dx = x - avg_x
    dy = y - avg_y

    x_l = math.cos(alpha) * dx - math.sin(alpha) * dy
    y_l = math.sin(alpha) * dx + math.cos(alpha) * dy

    res = y_l - (a * (x_l) ** 2)

    return res

# boundaries for a (width of parabola opening) and angle
bounds = ([0.001, -np.inf], [np.inf, np.inf])
p_x = (avg_x, max_line_1[0], max_line_2[0])
p_y = (avg_y, max_line_1[1], max_line_2[1])

x, y = np.nonzero(skeleton)
val = np.zeros(len(x))

# fit the parabola into the data points
param, param_cov = scipy.optimize.curve_fit(rotated_parabola, (y, x), val, method='dogbox', bounds=bounds)
print(param)
theta = param[1]


# this function was used to draw the parabola on the image
def sample_parabola(x_l, a, alpha, xc, yc):
    y_l = (a * (x_l) ** 2)
    x = math.cos(alpha) * x_l + math.sin(alpha) * y_l
    y = -math.sin(alpha) * x_l + math.cos(alpha) * y_l
    x += xc
    y += yc
    return x, y


x_arr = np.linspace(-300, 300, 300)
# create points of a parabola with parameters, calculated by curve_fit
x_p, y_p = sample_parabola(x_arr, param[0], param[1], avg_x, avg_y)
img_orig = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/Base34_edit/20051205_57723_0400_PP.tif', 0)

for x, y in zip(x_p, y_p):
    parab = cv2.drawMarker(img_orig, (int(x), int(y)), (250, 250, 250), markerType=cv2.MARKER_CROSS,
                           markerSize=5, thickness=2, line_type=cv2.LINE_AA)

parab = cv2.drawMarker(parab, (int(avg_x), int(avg_y)), (200, 0, 200), markerType=cv2.MARKER_CROSS,
                       markerSize=20, thickness=2, line_type=cv2.LINE_AA)

plt.imshow(parab)
plt.axis('off')
plt.show()

import math
import cv2
import numpy as np

skeleton = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/vessels_skeleton.png', 0)
height, width = skeleton.shape
# load the mask created in mask.py
mask = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/mask.png', 0)
resized = cv2.resize(mask, (height, width), interpolation=cv2.INTER_AREA)
# threshold the mask and create binary
thr, thr_img = cv2.threshold(resized, 11, 255, cv2.THRESH_BINARY)
# this is binary mask
opening = cv2.morphologyEx(thr_img, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
                           iterations=2)
# threshold and invert
for i in range(height):
    for j in range(width):
        if skeleton[i, j] < 250:
            skeleton[i, j] = 255
        else:
            skeleton[i, j] = 0

threshold = 10
theta = 5

# detect lines with Hough algorithm
lines = cv2.HoughLinesP(skeleton, rho=1, theta=np.pi / 180 * int(theta), threshold=int(threshold),
                        minLineLength=30, maxLineGap=2)
final_lines = []
intersection_points = []
final = skeleton

max_length_r = 0  # /
max_length_l = 0  # \
# first point for parabola - middle of this line #
max_line_r = 0  # /
max_line_l = 0  # \
# Draw the lines
if lines is not None:
    for i in range(0, len(lines)):
        l = lines[i][0]
        cv2.line(skeleton, (l[0], l[1]), (l[2], l[3]), (255 / 2, 255 / 2, 255 / 2), 1, cv2.LINE_AA)
        tg = (l[3] - l[1]) / (l[2] - l[0])
        angle = abs(np.arctan(tg)) * 57.3
        # leave only lines with angles between 45 and 135 degrees
        if 45 < angle < 135:
            final_lines.append(l)
            # find the longest line
            if 45 < angle < 85:  # /
                length_r = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
                if max_length_r < length_r:
                    max_length_r = length_r
                    max_line_r = l
            if 95 < angle < 135:
                length_l = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
                if max_length_l < length_l:
                    max_length_l = length_l
                    max_line_l = l

cv2.line(final, (max_line_r[0], max_line_r[1]), (max_line_r[2], max_line_r[3]),
         (255, 0, 255), 1, cv2.LINE_AA)
cv2.line(final, (max_line_l[0], max_line_l[1]), (max_line_l[2], max_line_l[3]),
         (255, 255, 0), 1, cv2.LINE_AA)

cv2.imshow('Lines', final)
cv2.waitKey()


def intersection(L1, L2):
    D = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    else:
        return False


marked = skeleton
all_x = []
all_y = []

# Calculate the intersection points
for line1 in final_lines:
    A1 = (line1[1] - line1[3])
    B1 = (line1[2] - line1[0])
    C1 = (line1[0] * line1[3] - line1[2] * line1[1])
    L1 = (A1, B1, -C1)
    for line2 in final_lines:
        if True:
            A2 = (line2[1] - line2[3])
            B2 = (line2[2] - line2[0])
            C2 = (line2[0] * line2[3] - line2[2] * line2[1])
            L2 = (A2, B2, -C2)
            R = intersection(L1, L2)
            if R and width > R[0] > 0 and height > R[1] > 0:
                mask = opening[int(R[0]), int(R[1])]
                if mask == 255:
                    all_x.append(R[0])
                    all_y.append(R[1])
            else:
                continue

# second point for parabola - intersection of lines #
avg_x = np.sum(all_x) / len(all_x)
avg_y = np.sum(all_y) / len(all_y)
marked = cv2.drawMarker(marked, (int(avg_x), int(avg_y)), (255 / 3, 255 / 3, 255 / 3), markerType=cv2.MARKER_CROSS,
                        markerSize=20, thickness=2, line_type=cv2.LINE_AA)

cv2.imshow('Intersections: OD detection', marked)
cv2.waitKey()

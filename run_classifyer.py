import os, sys
import joblib
import sklearn
import tqdm
import xgboost

sys.path.append('C://Users/Aleksandra/PycharmProjects/image_quality/EyeQuality_new')

from eye_quality import classify
clsf = classify.XGBClassifier('xgboost_binary_prob_all.pkl')

import glob
path = "C:/Users/Aleksandra/PycharmProjects/image_quality/Base34_edit"
# path = "D:/all-images"

samples = glob.glob(os.path.join(path, '*.tif'))

for s in samples:
    r = clsf.predict(s)
    # 0 - bad, 1 - good
    if r == 0: print(r, s)


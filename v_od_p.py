import cv2
import numpy as np
import sys
from PIL import Image
from matplotlib import cm
import matplotlib.pyplot as plt
from PIL.ImageQt import ImageQt
import math
import scipy

# Load the image
img = cv2.imread('C:/Users/Aleksandra/PycharmProjects/image_quality/Base34_edit/20051205_59651_0400_PP.tif',
                 cv2.IMREAD_GRAYSCALE)

height, width = img.shape

# apply mask
mask = np.where(img >= 15, 1, 0)
kernel = np.ones((6, 6), np.uint8)
opening_mask = cv2.morphologyEx(np.uint8(mask), cv2.MORPH_OPEN, kernel, iterations=2)
masked_img = img * opening_mask
image = Image.fromarray(masked_img)

# Blackhat morphology - create vessels skeleton
kernel_s = 15
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel_s, kernel_s))
blackhat = cv2.morphologyEx(masked_img, cv2.MORPH_BLACKHAT, kernel)
cv2.imshow('Blackhat',blackhat)
cv2.waitKey(0)

# threshold of blood vessels skeleton
thr, thr_img = cv2.threshold(blackhat, 10, 255, cv2.THRESH_BINARY)
erosion = cv2.erode(thr_img, (5, 5), iterations=3)
cv2.imshow('Erosion', erosion)
cv2.waitKey(0)

# Hough Lines algorithm #
# original code: test.py
skeleton = erosion

# threshold
for i in range(height):
    for j in range(width):
        if skeleton[i, j] < 250:
            skeleton[i, j] = 0
        else:
            skeleton[i, j] = 255

threshold = 10
theta = 5

# Detect lines with Hough algorithm
lines = cv2.HoughLinesP(erosion, rho=1, theta=np.pi / 180 * int(theta), threshold=int(threshold),
                        minLineLength=30, maxLineGap=2)
height, width = erosion.shape

final_lines = []
intersection_points = []
final = np.zeros(erosion.shape, dtype=int)

max_length = 0
max_line = 0
max_length_1 = 0
max_length_2 = 0
max_line_1 = 0
max_line_2 = 0
# store the middle points of the lines
mid_lines_x = []
mid_lines_y = []

if lines is not None:
    for i in range(0, len(lines)):
        l = lines[i][0]
        cv2.line(erosion, (l[0], l[1]), (l[2], l[3]), (255 / 2, 255 / 2, 255 / 2), 1, cv2.LINE_AA)
        tg = (l[3] - l[1]) / (l[2] - l[0])
        angle = math.atan(tg) * 57.3

        if angle < 0:
            angle = 180 - abs(angle)

        # leave only lines with angles between 45 and 135 degrees
        if 45 < angle < 135:
            final_lines.append(l)
            length = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
            # find the longest lines with angles 45-85 and 95-135
            if 45 < angle < 85:
                length_1 = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
                if length_1 > max_length_1:
                    max_length_1 = length_1
                    max_line_1 = l
            if 95 < angle < 135:
                length_2 = math.sqrt((l[2] - l[0]) ** 2 + (l[3] - l[1]) ** 2)
                if length_2 > max_length_2:
                    max_length_2 = length_2
                    max_line_2 = l

            # find the longest line
            if max_length < length:
                max_length = length
                max_line = l

# function to calculate intersection point of two lines
def intersection(L1, L2):
    D = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    else:
        return False

marked = erosion
all_x = []
all_y = []

# Calculate the intersection points for all lines
for line1 in final_lines:
    A1 = (line1[1] - line1[3])
    B1 = (line1[2] - line1[0])
    C1 = (line1[0] * line1[3] - line1[2] * line1[1])
    L1 = (A1, B1, -C1)
    for line2 in final_lines:
        if True:
            A2 = (line2[1] - line2[3])
            B2 = (line2[2] - line2[0])
            C2 = (line2[0] * line2[3] - line2[2] * line2[1])
            L2 = (A2, B2, -C2)
            R = intersection(L1, L2)
            # if the point of intersection exists and is inside the image
            if R:
                if width > R[0] > 0 and height > R[1] > 0:
                    all_x.append(R[0])
                    all_y.append(R[1])
            else:
                continue

# average intersection point
avg_x = np.sum(all_x) / len(all_x)
avg_y = np.sum(all_y) / len(all_y)

# Parabola algorithm - macula region detection #
# general equation for a rotated parabola
def parabola(X, theta_p, p):
    x, y = X
    res = ((x - avg_x) * math.sin(theta_p) + (y - avg_y) * math.cos(theta_p)) ** 2 - 2 * p * (
            (x - avg_x) * math.cos(theta_p) - (y - avg_y) * math.sin(theta_p))
    return res


# this function was used for parabola fitting
def rotated_parabola(X, a, alpha):
    x, y = X
    # translation
    dx = x - avg_x
    dy = y - avg_y
    # rotation
    x_l = math.cos(alpha) * dx - math.sin(alpha) * dy
    y_l = math.sin(alpha) * dx + math.cos(alpha) * dy
    # parabola
    res = y_l - (a * (x_l) ** 2)

    return res


# boundaries for a (width of parabola branches) and angle
bounds = ([0.001, -np.inf], [np.inf, np.inf])

p_x = (avg_x, max_line_1[0], max_line_2[0])
p_y = (avg_y, max_line_1[1], max_line_2[1])

x, y = np.nonzero(skeleton)
val = np.zeros(len(x))

# fit the parabola into the data points
param, param_cov = scipy.optimize.curve_fit(rotated_parabola, (y, x), val, method='dogbox', bounds=bounds)
# angle, focal, vertex x,y
print(param)
theta = param[1]

# this function was used to draw the parabola on the image
def sample_parabola(x_l, a, alpha, xc, yc):
    y_l = (a * x_l ** 2)
    x = math.cos(alpha) * x_l + math.sin(alpha) * y_l
    y = -math.sin(alpha) * x_l + math.cos(alpha) * y_l
    x += xc
    y += yc
    return x, y


x_arr = np.linspace(-300, 300, 300)
# create points of a parabola with parameters, calculated by curve_fit
x_p, y_p = sample_parabola(x_arr, param[0], param[1], avg_x, avg_y)
img_orig = img

for x, y in zip(x_p, y_p):
    parab = cv2.drawMarker(img_orig, (int(x), int(y)), (250, 250, 250), markerType=cv2.MARKER_CROSS,
                           markerSize=5, thickness=2, line_type=cv2.LINE_AA)

parab = cv2.drawMarker(parab, (int(avg_x), int(avg_y)), (200, 0, 200), markerType=cv2.MARKER_CROSS,
                       markerSize=20, thickness=2, line_type=cv2.LINE_AA)

cv2.imshow('image', parab)
cv2.waitKey(0)
cv2.imwrite('parabola_mess6.png', parab)

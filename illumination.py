import numpy as np
import cv2
from skimage import io

nat = io.imread('blur_test_pics/merged_365.png')
nat_2 = cv2.cvtColor(nat, cv2.COLOR_BGR2RGB)
cv2.imshow('gray', nat_2)
cv2.waitKey(0)


def gammaCorrection(src, gamma):
    invGamma = 1 / gamma
    table = [((i / 255) ** invGamma) * 255 for i in range(256)]
    table = np.array(table, np.uint8)
    return cv2.LUT(src, table)


gamma = 2.5  # change the value here to get different result
adjusted = gammaCorrection(nat_2, gamma=gamma)
cv2.imshow('adjusted', adjusted)
cv2.waitKey(0)

